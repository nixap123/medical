<?php

/*

type: layout
content_type: static
name: Contact Us

description: Contact us layout
position: 7
*/


?>
<?php include template_dir() . "header.php"; ?>

    <div class="edit" rel="content" field="power_content">
        <module type="layouts" template="skin-1"/>
        <module type="layouts" template="skin-10"/>
    </div>

<?php include template_dir() . "footer.php"; ?>