<?php include template_dir() . "header.php"; ?>

<module type="layouts" template="skin-1" />

<div class="page-section section pt-60 pb-45">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <!-- Blog Item -->
                <div class="blog-item mb-75">
                    <!-- Image -->
                    <div class="row">
                        <module data-type="pictures" data-template="default-post" rel="content"/>
                        <br /><br />
                    </div>
                    <div class="clearfix"></div>
                    <!-- Content -->
                    <div class="content fix">
                        <!-- Date -->
                        <span class="date">
                            <span class="day"><?php print date('d', strtotime(content_date())); ?></span>
                            <span class="month">/ <?php print date('M', strtotime(content_date())); ?></span>
                        </span>
                        <!-- Title -->
                        <h4 class="edit title" field="title" rel="content">If you are going to use a pass.</h4>

                        <div class="edit" field="content" rel="content">
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration some form, by injected humour, or randomised words which don't
                                look even slightly believable. Ifyouare going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden i the middletext.There are
                                many variations of passages of Lorem Ipsum available, but the majority hsuffered alteration in some form, by injected humour, There are many variations of passages of
                                Lorem Ipsum able, but the majority have suffered alteration in some form. there isn't anything embarrassing hidden.</p>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <module type="facebook_like" show-faces="false" layout="box_count">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="edit" rel="content" field="comments">
                                    <module data-type="comments" data-template="default" data-content-id="<?php print CONTENT_ID; ?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include template_dir() . "footer.php"; ?>
