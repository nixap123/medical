<!DOCTYPE HTML>
<html prefix="og: http://ogp.me/ns#" class="no-js" lang="en">
<head>
    <title>{content_meta_title}</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta property="og:title" content="{content_meta_title}">
    <meta name="keywords" content="{content_meta_keywords}">
    <meta name="description" content="{content_meta_description}">
    <meta property="og:type" content="{og_type}">
    <meta property="og:url" content="{content_url}">
    <meta property="og:image" content="{content_image}">
    <meta property="og:description" content="{og_description}">
    <meta property="og:site_name" content="{og_site_name}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <script>
        mw.lib.require('bootstrap3');
        mw.lib.require('slick');
    </script>

    <script>mw.lib.require('material_icons');</script>

    <!-- Icon Font -->
    <link rel="stylesheet" href="{TEMPLATE_URL}css/pe-icon-7-stroke.css">
    <!-- Plugins css file -->
    <link rel="stylesheet" href="{TEMPLATE_URL}css/plugins.css">
    <!-- Theme main style -->
    <link rel="stylesheet" href="{TEMPLATE_URL}css/style.css">
    <!-- Responsive css -->
    <link rel="stylesheet" href="{TEMPLATE_URL}css/responsive.css">

    <!-- Modernizr JS -->
    <script src="{TEMPLATE_URL}js/vendor/modernizr-2.8.3.min.js"></script>


    <link rel="stylesheet" href="{TEMPLATE_URL}modules/layouts/templates/layouts.css" type="text/css" media="all">
    <?php $color_scheme = get_option('color-scheme', 'mw-template-power'); ?>
    <?php if ($color_scheme != '' AND $color_scheme != 'orange'): ?>
        <link rel="stylesheet" href="{TEMPLATE_URL}css/<?php print $color_scheme; ?>.css" type="text/css" media="all">
    <?php endif; ?>


    <script>
        AddToCartModalContent = window.AddToCartModalContent || function (title) {
                var html = ''

                    + '<section style="text-align: center;">'
                    + '<h5>' + title + '</h5>'
                    + '<p><?php _e("has been added to your cart"); ?></p>'
                    + '<a href="javascript:;" onclick="mw.tools.modal.remove(\'#AddToCartModal\')" class="btn btn-default" style="margin-bottom: 10px;"><?php _e("Continue shopping"); ?></a>'
                    + '<a href="<?php print checkout_url(); ?>" class="btn btn-default"><?php _e("Checkout"); ?></a></section>';

                return html;
            }
    </script>
    <script>mw.lib.require('mw_icons_mind');</script>

</head>
<body class="<?php print helper_body_classes(); ?>">
<div class="wrapper">

    <header class="header-section section sticker">
        <div class="container" field="power_header" rel="global">
            <div class="row">
                <div class="col-xs-12">
                    <!-- logo -->
                    <div class="header-logo float-left">
                        <module type="logo" id="logo_header" default-text="Cashy" class="pull-left"/>
                    </div>
                    <!-- header-search & total-cart -->
                    <div class="float-right">
                        <div class="header-option-btns float-right">
                            <!-- header-search -->
                            <div class="header-search float-left">
                                <button class="search-toggle" data-toggle="dropdown"><i class="pe-7s-search"></i></button>
                                <div class="dropdown-menu header-search-form">
                                    <form action="<?php print site_url(); ?>search.php">
                                        <input type="text" name="keywords" placeholder="Search">
                                        <button><i class="fa fa-long-arrow-right"></i></button>
                                    </form>
                                </div>
                            </div>
                            <!-- header Account -->
                            <div class="header-account float-left">
                                <ul>
                                    <li><a href="#" data-toggle="dropdown"><i class="pe-7s-config"></i></a>
                                        <ul class="dropdown-menu">
                                            <?php if (!is_logged()): ?>
                                                <li><a href="<?php print login_url(); ?>"><?php _e('Log in'); ?></a></li>
                                                <li><a href="<?php print register_url(); ?>"><?php _e('Register'); ?></a></li>
                                            <?php else: ?>
                                                <li><a href="<?php print logout_url(); ?>"><?php _e('Log out'); ?></a></li>
                                            <?php endif; ?>
                                            <li><a href="<?php print checkout_url(); ?>"><?php _e('Checkout'); ?></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <!-- Header Cart -->


                            <?php $shopping_cart = get_option('shopping-cart', 'mw-template-power'); ?>
                            <?php if ($shopping_cart == 'true'): ?>
                                <module type="shop/cart" template="small" id="cart-bag" class="header-cart float-left"/>
                            <?php endif; ?>

                        </div>

                        <module type="menu" name="header_menu" id="main-navigation" template="navbar" class="main-menu menu-right float-right"/>
                    </div>
                    <div class="mobile-menu"></div>
                </div>
            </div>
        </div>
    </header>