<?php

/*

type: layout
content_type: static
name: Register
description: Register layout

*/


?>
<?php include template_dir() . "header.php"; ?>

    <div class="edit" rel="content" field="power_content">
        <module type="layouts" template="skin-1"/>
        <module type="layouts" template="skin-8"/>
    </div>

<?php include template_dir() . "footer.php"; ?>