function init() {

    /*--
     Menu Sticky
     -----------------------------------*/
    var $window = $(window);
    $window.on('scroll', function () {
        var scroll = $window.scrollTop();
        if (scroll < 300) {
            $(".sticker").removeClass("stick");
        } else {
            $(".sticker").addClass("stick");
        }
    });
    /*--
     Mobile Menu
     -----------------------------------*/
    $('.main-menu').meanmenu({
        meanScreenWidth: '991',
        meanMenuContainer: '.mobile-menu',
        meanMenuClose: '<i class="pe-7s-close-circle"></i>',
        meanMenuOpen: '<i class="pe-7s-menu"></i>',
        meanRevealPosition: 'right',
        meanMenuCloseSize: '30px',
    });

    /*--
     Nivo Slider
     -----------------------------------*/
    $('#home-slider').nivoSlider({
        directionNav: true,
        animSpeed: 1000,
        effect: 'random',
        slices: 18,
        pauseTime: 88885000,
        pauseOnHover: false,
        controlNav: false,
        prevText: '<i class="fa fa-long-arrow-left"></i>',
        nextText: '<i class="fa fa-long-arrow-right"></i>'
    });

    /*--
     Home Slick Slider
     -----------------------------------*/
    // /*-- Image Slider --*/
    // $('.home-slick-image-slider').slick({
    //     asNavFor: '.home-slick-text-slider',
    //     slidesToShow: 1,
    //     prevArrow: '<button type="button" class="arrow-prev"><i class="fa fa-long-arrow-left"></i></button>',
    //     nextArrow: '<button type="button" class="arrow-next"><i class="fa fa-long-arrow-right"></i></button>',
    //     responsive: [
    //         {
    //             breakpoint: 767,
    //             settings: {
    //                 arrows: false,
    //                 autoplay: true,
    //                 autoplaySpeed: 5000,
    //             }
    //         },
    //     ]
    // });
    // /*-- Text Slider --*/
    // $('.home-slick-text-slider').slick({
    //     arrows: false,
    //     asNavFor: '.home-slick-image-slider',
    //     slidesToShow: 1,
    // });

    /*--
     Isotop with ImagesLoaded
     -----------------------------------*/
    var productFilter = $('.isotope-product-filter');
    var productGrid = $('.isotope-grid');
    /*-- Images Loaded --*/
    productGrid.imagesLoaded(function () {
        /*-- Filter List --*/
        productFilter.on('click', 'button', function () {
            productFilter.find('button').removeClass('active');
            $(this).addClass('active');
            var filterValue = $(this).attr('data-filter');
            productGrid.isotope({filter: filterValue});
        });
        /*-- Filter Grid --*/
        productGrid.isotope({
            itemSelector: '.isotope-item',
            masonry: {
                columnWidth: '.isotope-item',
            }
        });
    });

    /*--
     Price Range
     -----------------------------------*/
    $('#price-range').slider({
        range: true,
        min: 0,
        max: 700,
        values: [70, 500],
        slide: function (event, ui) {

            $('.price-amount').val('€' + ui.values[0] + ' - €' + ui.values[1]);

        }
    });
    $('.price-amount').val('€' + $('#price-range').slider('values', 0) +
        ' - €' + $('#price-range').slider('values', 1));

    /*--
     Product Quantity
     -----------------------------------*/
    // $('.product-quantity').append('');
    $('.qtybtn').on('click', function () {
        var $button = $(this);
        var oldValue = $button.parent().find('input').val();
        if ($button.hasClass('inc')) {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below zero
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }
        $button.parent().find('input').val(newVal).trigger('change');
    });

    /*--
     Checkout Form Collapse on Checkbox
     -----------------------------------*/
    $('.checkout-form input[type="checkbox"]').on('click', function () {
        var $collapse = $(this).data('target');
        if ($(this).is(':checked')) {
            $('.collapse[data-collapse="' + $collapse + '"]').slideDown();
        } else {
            $('.collapse[data-collapse="' + $collapse + '"]').slideUp();
        }
    })

    /*--
     Product Filter Toggle
     -----------------------------------*/
    $('.product-filter-toggle').on('click', function () {
        $('.product-filter-wrapper').slideToggle();
    })

    /*--
     ScrollUp
     -----------------------------------*/
    $.scrollUp({
        scrollText: '<i class="fa fa-angle-up"></i>',
        easingType: 'linear',
        scrollSpeed: 900,
        animation: 'fade'
    });

    /*--
     WOW
     -----------------------------------*/
    new WOW().init();

    $('.quick-view-modal').on('click', function () {
        var thisPageUrl = $(this).data('url');
        var ajaxLoading = '<br /><br /><br /> <svg xmlns="http://www.w3.org/2000/svg" width="200px" height="200px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-fidget-spinner"> <g transform="rotate(34.1054 50 50)"> <g transform="translate(50 50)"> <g ng-attr-transform="scale({{config.r}})" transform="scale(0.8)"> <g transform="translate(-50 -58)"> <path ng-attr-fill="{{config.c2}}" d="M27.1,79.4c-1.1,0.6-2.4,1-3.7,1c-2.6,0-5.1-1.4-6.4-3.7c-2-3.5-0.8-8,2.7-10.1c1.1-0.6,2.4-1,3.7-1c2.6,0,5.1,1.4,6.4,3.7 C31.8,72.9,30.6,77.4,27.1,79.4z" fill="#0a0a0a"/> <path ng-attr-fill="{{config.c3}}"d="M72.9,79.4c1.1,0.6,2.4,1,3.7,1c2.6,0,5.1-1.4,6.4-3.7c2-3.5,0.8-8-2.7-10.1c-1.1-0.6-2.4-1-3.7-1c-2.6,0-5.1,1.4-6.4,3.7 C68.2,72.9,69.4,77.4,72.9,79.4z" fill="#ffffff"/> <circle ng-attr-fill="{{config.c4}}" cx="50" cy="27" r="7.4" fill="#f9ae5c"/> <path ng-attr-fill="{{config.c1}}"d="M86.5,57.5c-3.1-1.9-6.4-2.8-9.8-2.8c-0.5,0-0.9,0-1.4,0c-0.4,0-0.8,0-1.1,0c-2.1,0-4.2-0.4-6.2-1.2 c-0.8-3.6-2.8-6.9-5.4-9.3c0.4-2.5,1.3-4.8,2.7-6.9c2-2.9,3.2-6.5,3.2-10.4c0-10.2-8.2-18.4-18.4-18.4c-0.3,0-0.6,0-0.9,0 C39.7,9,32,16.8,31.6,26.2c-0.2,4.1,1,7.9,3.2,11c1.4,2.1,2.3,4.5,2.7,6.9c-2.6,2.5-4.6,5.7-5.4,9.3c-1.9,0.7-4,1.1-6.1,1.1 c-0.4,0-0.8,0-1.2,0c-0.5,0-0.9-0.1-1.4-0.1c-3.1,0-6.3,0.8-9.2,2.5c-9.1,5.2-12,17-6.3,25.9c3.5,5.4,9.5,8.4,15.6,8.4 c2.9,0,5.8-0.7,8.5-2.1c3.6-1.9,6.3-4.9,8-8.3c1.1-2.3,2.7-4.2,4.6-5.8c1.7,0.5,3.5,0.8,5.4,0.8c1.9,0,3.7-0.3,5.4-0.8 c1.9,1.6,3.5,3.5,4.6,5.7c1.5,3.2,4,6,7.4,8c2.9,1.7,6.1,2.5,9.2,2.5c6.6,0,13.1-3.6,16.4-10C97.3,73.1,94.4,62.5,86.5,57.5z M29.6,83.7c-1.9,1.1-4,1.6-6.1,1.6c-4.2,0-8.4-2.2-10.6-6.1c-3.4-5.9-1.4-13.4,4.5-16.8c1.9-1.1,4-1.6,6.1-1.6 c4.2,0,8.4,2.2,10.6,6.1C37.5,72.8,35.4,80.3,29.6,83.7z M50,39.3c-6.8,0-12.3-5.5-12.3-12.3S43.2,14.7,50,14.7 c6.8,0,12.3,5.5,12.3,12.3S56.8,39.3,50,39.3z M87.2,79.2c-2.3,3.9-6.4,6.1-10.6,6.1c-2.1,0-4.2-0.5-6.1-1.6 c-5.9-3.4-7.9-10.9-4.5-16.8c2.3-3.9,6.4-6.1,10.6-6.1c2.1,0,4.2,0.5,6.1,1.6C88.6,65.8,90.6,73.3,87.2,79.2z"fill="#28292f"/> </g> </g> </g> <animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"/> </g> </svg> <br /><br /><br />';

        $('#quickViewModal #quick-view-content').html(ajaxLoading);

        $.get(thisPageUrl, function (data) {
            var parser = $('<div>');
            parser.html(data);

            $('#quickViewModal #quick-view-content').html(parser.find('.page-section').html());
            init();
        })
    });

}

$(document).ready(function () {
    init();
});
