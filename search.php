<?php include template_dir() . "header.php"; ?>

<div class="page-section section pt-80 pb-120">
    <div class="container">

        <div class="row">
            <?php
            $keywords = '';
            if (isset($_GET['keywords'])) {
                $keywords = htmlspecialchars($_GET['keywords']);
            }
            ?>
            <h1>Results for <?php print $keywords; ?></h1>
            <module type="shop/products" limit="18" keyword="<?php print $keywords; ?>" description-length="70"/>
        </div>

    </div>
</div>


<?php include template_dir() . "footer.php"; ?>
