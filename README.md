#Power

Power up your online existence and penetration by having an ultimate website template covering your company information, blog, online store, contact forms, and others.

Power is a robust and powerful multipurpose web template with more than 10 ready page designs which supports a wide range of Microweber Modules. All this makes Power a good fit for any Corporate, Agency, Startup, Personal and many other types of websites.

##Features:

* +10 Page Templates (Home, Services, Portfolio, Blog ... others)
* 75 Modules included
* eCommerce Support  (Shop Page, Products Feed, Shopping Cart and Checkout Page)
* Pixel Perfect Design
* Fully Responsive Layout*
* User-Friendly Code
* Clean Markup
* Creative Design
* Multiple Skins Sliders
* Fully Responsive
* Cross Browser Support
* Easy to Customize
* Well Documented
* Free Lifetime Updates
* Community Support
