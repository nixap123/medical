<?php
/*
type: layout

name: Default

description: Add to cart

*/

?>

<div class="">
    <?php
    if (isset($params['content-id'])) {
        $product = get_content_by_id($params["content-id"]);
        $title = $product['title'];
    } else {
        $title = _e("Product", true);
    }
    ?>


    <module type="custom_fields" data-content-id="<?php print intval($for_id); ?>" data-skip-type="price" id="cart_fields_<?php print $params['id'] ?>"/>


    <div class="quantity-cart section">
        <div class="product-quantity">
            <input type="text" name="qty" value="1"/>
        </div>

        <?php if (is_array($data)): ?>
            <?php $i = 1; ?>
            <?php foreach ($data as $key => $v): ?>
                <?php if (!isset($in_stock) or $in_stock == false) : ?>
                    <button class="add-to-cart" type="button" disabled="disabled"
                            onclick="Alert('<?php print addslashes(_e("This item is out of stock and cannot be ordered", true)); ?>');"><i
                                class="icon-shopping-cart glyphicon glyphicon-shopping-cart"></i>
                        <?php _e("Out of stock"); ?>
                    </button>
                <?php else: ?>
                    <button class="add-to-cart" type="button"
                            onclick="mw.cart.add('.mw-add-to-cart-<?php print $params['id'] ?>','<?php print $v ?>', '<?php print $title; ?>');"><i
                                class="icon-shopping-cart glyphicon glyphicon-shopping-cart"></i>
                        <?php _e($button_text !== false ? $button_text : "Add to cart"); ?>
                    </button>
                    <?php $i++; endif; ?>
                <?php if ($i > 1) : ?>
                    <br/>
                <?php endif; ?>
                <?php $i++;
            endforeach; ?>
        <?php endif; ?>
    </div>


</div>
