<?php

/*

type: layout

name: Big

description: Full width cart template

*/

?>

<script>
    $(document).ready(function () {
        $(".mw-shopping-cart-big-layout-1 .mw-qty-field .cartDecreaseProductsNumber").click(function () {
            var thisQty = $(this).parent().find('input');
            var inputVal = thisQty.val();

            if (inputVal < 2) {
                inputVal = 1;
                thisQty.val(inputVal);
            } else {
                inputVal = inputVal - 1;
                thisQty.val(inputVal);
            }
            thisQty.trigger('change');
        });

        $(".mw-shopping-cart-big-layout-1 .mw-qty-field .cartIncreaseProductsNumber").click(function () {
            var thisQty = $(this).parent().find('input');
            var inputVal = thisQty.val();
            inputVal = parseInt(inputVal) + (1);
            thisQty.val(inputVal);
            thisQty.trigger('change');
        });
    });
</script>

<div class="mw-cart-<?php print $params['id'] ?> <?php print  $template_css_prefix; ?>">
    <div class="col-xs-12 mw-cart-<?php print $params['id'] ?>">
        <h1 class="edit" rel="<?php print $params['id'] ?>" field="cart_title">
            <?php _e('My cart'); ?>
        </h1>

        <?php if (is_array($data)) : ?>
            <div class="cart-table table-responsive mb-40">

                <table class="table">
                    <thead>
                    <tr>
                        <th colspan="2" class="mw-cart-table-product"><?php _e("Product"); ?></th>
                        <th class="right"><?php _e("Price"); ?></th>
                        <th class="center" style="min-width:130px;"><?php _e("Quantity"); ?></th>
                        <th class="right"><?php _e("Total"); ?></th>
                        <th class="right"><?php _e("Remove"); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $total = cart_sum();
                    foreach ($data as $item) : ?>
                        <tr class="mw-cart-item mw-cart-item-<?php print $item['id'] ?>">
                            <td class="pro-thumbnail">
                                <?php if (isset($item['item_image']) and $item['item_image'] != false): ?>
                                    <?php $p = $item['item_image']; ?>
                                <?php else: ?>
                                    <?php $p = get_picture($item['rel_id']); ?>
                                <?php endif; ?>
                                <?php if ($p != false): ?>
                                    <img class="img-responsive mw-order-item-image mw-order-item-image-<?php print $item['id']; ?>" src="<?php print thumbnail($p, 388, 442); ?>"/>
                                <?php endif; ?>
                            </td>
                            <td class="pro-title">
                                <?php print $item['title'] ?>
                                <?php if (isset($item['custom_fields'])): ?>
                                    <?php print $item['custom_fields'] ?>
                                <?php endif ?>
                            </td>
                            <td class="pro-price"><?php print currency_format($item['price']); ?></td>
                            <td class="pro-quantity">
                                <div class="product-quantity">
                                    <input type="text" name="qty" value="<?php print $item['qty'] ?>" onchange="mw.cart.qty('<?php print $item['id'] ?>', this.value)"/>
                                    <span class="dec qtybtn"><i class="fa fa-angle-left"></i></span><span class="inc qtybtn"><i class="fa fa-angle-right"></i></span>
                                </div>
                            </td>
                            <td class="pro-subtotal"><?php print currency_format($item['price'] * $item['qty']); ?></td>
                            <td class="pro-remove"><a title="<?php _e("Remove"); ?>" class="remove tip" href="javascript:mw.cart.remove('<?php print $item['id'] ?>');"
                                                      data-tip="<?php _e("Remove"); ?>"
                                                      data-tipposition="top-center">×</a></td>
                        </tr>
                    <?php endforeach; ?>

                    <script></script>
                    </tbody>
                </table>
            </div>


            <div class="row">
                <div class="col-md-8 col-sm-7 col-xs-12">
                    <module type="shop/coupons"/>
                </div>
                <div class="col-md-4 col-sm-5 col-xs-12">


                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <h4>Shipping information</h4>
                                <hr/>
                            </div>
                        </div>
                        <?php if (is_array($shipping_options)) : ?>
                            <div <?php if (!$show_shipping_stuff) : ?> style="display:none" <?php endif; ?>>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <?php _e("Shipping to"); ?>:
                                    </div>
                                    <div class="col-xs-6 right">
                                        <module type="shop/shipping" view="select"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-6"><?php _e("Shipping price"); ?>:</div>
                                <div class="col-xs-6 right">
                                    <module type="shop/shipping" view="cost"/>
                                </div>
                            </div>

                            <?php if (function_exists('cart_get_tax') and get_option('enable_taxes', 'shop') == 1) : ?>
                                <div class="row">
                                    <div class="col-xs-6"><?php _e("Tax"); ?>:</div>
                                    <div class="col-xs-6 right">
                                        <?php print currency_format(cart_get_tax()); ?>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <div class="row">
                                <div class="col-xs-12">
                                    <hr/>
                                </div>

                                <div class="col-xs-6 total-lable"><?php _e("Total Price"); ?>:</div>
                                <div class="col-xs-6 right total-price">
                                    <?php print currency_format($print_total); ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

        <?php else : ?>
            <h4 class="alert alert-warning">
                <?php _e("Your cart is empty."); ?>
            </h4>
        <?php endif; ?>
    </div>
</div>
