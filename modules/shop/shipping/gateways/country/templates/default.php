<?php

/*

type: layout

name: Default

description: Default

*/
?>

<div class="<?php print $config['module_class'] ?>">
    <div id="<?php print $rand; ?>">
        <?php $selected_country = mw()->user_manager->session_get('shipping_country'); ?>

        <div class="row">
            <div class="col-sm-6 col-xs-12 mb-30">
                <label for="country"><?php _e("Country"); ?></label>
                <select name="country">
                    <option value=""><?php _e("Country"); ?></option>
                    <?php foreach ($data as $item): ?>
                        <option value="<?php print $item['shipping_country'] ?>" <?php if (isset($selected_country) and $selected_country == $item['shipping_country']): ?> selected="selected" <?php endif; ?>><?php print $item['shipping_country'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="col-sm-6 col-xs-12 mb-30">
                <label for="city"><?php _e("Town / City"); ?></label>
                <input name="Address[city]" type="text" value=""/>
            </div>

            <div class="col-sm-6 col-xs-12 mb-30">
                <label for="state"><?php _e("State / Province"); ?></label>
                <input name="Address[state]" class="" type="text" value=""/>
            </div>

            <div class="col-sm-6 col-xs-12 mb-30">
                <label for="zip"><?php _e("ZIP / Postal Code"); ?></label>
                <input name="Address[zip]" type="text" value=""/>
            </div>

            <div class="col-xs-12 mb-30">
                <label for="address"><?php _e("Address"); ?></label>
                <input name="Address[address]" type="text" value="" placeholder="Street address, Floor, Apartment, etc..."/>
            </div>

            <div class="col-xs-12 mb-30 order-notes">
                <label for="note"><?php _e("Note"); ?></label>
                <textarea name="other_info" id="note" placeholder="Additional Information ( Special notes for delivery - Optional )"></textarea>
            </div>
        </div>

    </div>
</div>
