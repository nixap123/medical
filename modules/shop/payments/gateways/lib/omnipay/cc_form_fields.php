<div class="col-xs-12 col-sm-6 pb-20">
    <label class="control-label">
        <?php _e("First Name"); ?>
    </label>
    <input name="cc_first_name" type="text" value=""/>
</div>
<div class="col-xs-12 col-sm-6 pb-20">
    <label class="control-label">
        <?php _e("Last Name"); ?>
    </label>
    <input name="cc_last_name" type="text" value=""/>
</div>
<div class="col-xs-12 col-sm-6 pb-20">
    <label class="control-label">
        <?php _e("Credit Card"); ?>
    </label>
    <select name="cc_type">
        <option value="Visa" selected>
            <?php _e("Visa"); ?>
        </option>
        <option value="MasterCard">
            <?php _e("MasterCard"); ?>
        </option>
        <option value="Discover">
            <?php _e("Discover"); ?>
        </option>
        <option value="Amex">
            <?php _e("American Express"); ?>
        </option>
    </select>
</div>
<div class="col-xs-12 col-sm-6 pb-20">
    <label>
        <?php _e("Credit Card Number"); ?>
    </label>
    <input name="cc_number" type="text" value=""/>
</div>
<div class="col-xs-12 pb-20">
    <label style="font-weight: bold;">
        <?php _e("Expiration Date"); ?>
    </label>
    <div class="row m-t-0">
        <div class="col-xs-12 col-sm-6 ">
            <label><?php _e("Month"); ?></label>
            <input name="cc_month" placeholder="" type="text" value=""/>
        </div>
        <div class="col-xs-12 col-sm-6 ">
            <label><?php _e("Year"); ?></label>
            <input name="cc_year" placeholder="" type="text" value=""/>
        </div>
    </div>
</div>
<div class="col-xs-12 pb-20">
    <label>
        <?php _e("Verification Code"); ?>
    </label>
    <input name="cc_verification_value" type="text" value=""/>
    <div class="cc_process_error"></div>
</div>