<?php

/*

type: layout

name: Default

description: Default discounts template

*/

?>

<div class="cart-coupon mb-40">
    <h4>Coupon</h4>
    <p>Enter your coupon code if you have one.</p>
    <input type="text" name="promocode" placeholder="Coupon code"/>
    <input type="submit" value="Apply Coupon"/>
</div>
