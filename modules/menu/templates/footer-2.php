<?php

/*

type: layout

name: Footer-2

description: Navigation bar

*/

?>

<style>
    .footer-nav li {
        display: inline;
        margin: 0 20px
    }
</style>

<?php
$menu_filter['ul_class'] = 'footer-nav';
$menu_filter['ul_class_deep'] = '';
$menu_filter['li_class'] = '';
$menu_filter['a_class'] = '';


$mt = menu_tree($menu_filter);

if ($mt != false) {
    print ($mt);
} else {
    print lnotif("There are no items in the menu <b>" . $params['menu-name'] . '</b>');
}
?>
