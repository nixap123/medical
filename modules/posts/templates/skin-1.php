<?php

/*

type: layout

name: Home Posts

description: Skin 1

*/
?>

<?php if (!empty($data)): ?>
    <?php foreach ($data as $item): ?>
        <div class="col-md-6 col-xs-12  mb-40" itemscope itemtype="<?php print $schema_org_item_type_tag ?>">
            <div class="home-blog-item">
                <?php if (!isset($show_fields) or $show_fields == false or in_array('thumbnail', $show_fields)): ?>
                    <a href="<?php print $item['link'] ?>" itemprop="url" class="image float-left" style="background-image: url('<?php print thumbnail($item['image'], 400); ?>');"></a>
                <?php endif; ?>
                <!-- Content -->
                <div class="content fix">
                    <span class="date">
                        <span class="day"><?php print date('d', strtotime($item['created_at'])) ?></span>
                        <span class="month">/ <?php print date('M', strtotime($item['created_at'])) ?></span>
                    </span>
                    <!-- Title -->
                    <?php if (!isset($show_fields) or $show_fields == false or in_array('title', $show_fields)): ?>
                        <h4 class="title"><a href="<?php print $item['link'] ?>" itemprop="url"><?php print $item['title'] ?></a></h4>
                    <?php endif; ?>

                    <?php if (!isset($show_fields) or $show_fields == false or in_array('description', $show_fields)): ?>
                        <p itemprop="headline"><?php print $item['description'] ?></p>
                    <?php endif; ?>
                    <a href="<?php print $item['link'] ?>" itemprop="url" class="read-more"><?php _e('READ MORE'); ?></a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>

<?php if (isset($pages_count) and $pages_count > 1 and isset($paging_param)): ?>
    <?php print paging("num={$pages_count}&paging_param={$paging_param}&current_page={$current_page}") ?>
<?php endif; ?>
