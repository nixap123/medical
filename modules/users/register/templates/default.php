<?php

/*

type: layout

name: Default

description: Default register template

*/

?>
<script type="text/javascript">
    mw.moduleCSS("<?php print modules_url(); ?>users/users_modules.css");
    mw.require('forms.js', true);
    mw.require('url.js', true);
    $(document).ready(function () {
        mw.$('#user_registration_form_holder').submit(function () {
            mw.form.post(mw.$('#user_registration_form_holder'), '<?php print site_url('api') ?>/user_register', function () {
                mw.response('#register_form_holder', this);
                if (typeof this.success !== 'undefined') {
                    mw.form.post(mw.$('#user_registration_form_holder'), '<?php print site_url('api') ?>/user_login', function () {
                        mw.load_module('users/login', '#<?php print $params['id'] ?>');
                        window.location.href = window.location.href;
                    });
                }
            });
            return false;
        });
    });
</script>

<div class="module-register checkout-form">
    <div class="col-xs-12">
        <h2><?php _e("New Registration"); ?></h2>
    </div>
    <div id="register_form_holder">

        <div class="clearfix"></div>
        <div class="col-xs-12">
            <div class="alert" style="margin: 0 0 15px 0;display: none;"></div>
        </div>
        <div class="clearfix"></div>


        <form id="user_registration_form_holder" method="post" class="reg-form-clearfix">

            <?php print csrf_form(); ?>

            <div class="col-xs-12 pb-20">
                <label for="email"><?php _e("Email"); ?></label>
                <input type="text" id="email" name="email"/>
            </div>

            <?php if ($form_show_first_name): ?>
                <div class="col-xs-12 pb-20">
                    <label><?php _e("First name"); ?></label>
                    <input type="text" name="first_name" placeholder="">
                </div>
            <?php endif; ?>

            <?php if ($form_show_last_name): ?>
                <div class="col-xs-12 pb-20">
                    <label><?php _e("Last name"); ?></label>
                    <input type="text" name="last_name" placeholder="">
                </div>
            <?php endif; ?>

            <div class="col-xs-12 pb-20">
                <label><?php _e("Password"); ?></label>
                <input type="password" name="password" placeholder="">
            </div>

            <?php if ($form_show_password_confirmation): ?>
                <div class="col-xs-12 pb-20">
                    <label><?php _e("Repeat password"); ?></label>
                    <input type="password" name="password2" placeholder="">
                </div>
            <?php endif; ?>


            <module type="captcha" />

            <div class="col-xs-12 col-sm-6 pb-20">
                <div class="social-login">

                    <?php
                    # Login Providers
                    $facebook = get_option('enable_user_fb_registration', 'users') == 'y';
                    $twitter = get_option('enable_user_twitter_registration', 'users') == 'y';
                    $google = get_option('enable_user_google_registration', 'users') == 'y';
                    $windows = get_option('enable_user_windows_live_registration', 'users') == 'y';
                    $github = get_option('enable_user_github_registration', 'users') == 'y';
                    if ($facebook or $twitter or $google or $windows or $github) {
                        $have_social_login = true;
                    } else {
                        $have_social_login = false;
                    }
                    ?>
                    <?php if ($have_social_login) { ?><h5><?php _e("Login with"); ?>:</h5><?php } ?>
                    <?php if ($have_social_login){ ?>
                    <ul>
                        <?php } ?>
                        <?php if ($facebook): ?>
                            <li><a href="<?php print api_link('user_social_login?provider=facebook') ?>" class="mw-signin-with-facebook">Facebook login</a></li>
                        <?php endif; ?>
                        <?php if ($twitter): ?>
                            <li><a href="<?php print api_link('user_social_login?provider=twitter') ?>" class="mw-signin-with-twitter">Twitter login</a></li>
                        <?php endif; ?>
                        <?php if ($google): ?>
                            <li><a href="<?php print api_link('user_social_login?provider=google') ?>" class="mw-signin-with-google">Google login</a></li>
                        <?php endif; ?>
                        <?php if ($windows): ?>
                            <li><a href="<?php print api_link('user_social_login?provider=live') ?>" class="mw-signin-with-live">Windows login</a></li>
                        <?php endif; ?>
                        <?php if ($github): ?>
                            <li><a href="<?php print api_link('user_social_login?provider=github') ?>" class="mw-signin-with-github">Github login</a></li>
                        <?php endif; ?>
                        <?php if ($have_social_login){ ?>
                    </ul>
                <?php } ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 pb-20">
                <button type="submit" class="btn btn-default pull-right"><?php print $form_btn_title ?></button>
            </div>
            <div style="clear: both"></div>
        </form>

    </div>
</div>
