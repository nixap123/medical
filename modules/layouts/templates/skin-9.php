<?php

/*

type: layout

name: Blog Posts

position: 9

*/
?>

<div class="page-section section pt-60 pb-80 edit safe-mode nodrop" field="layout-skin-9-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <module data-type="posts" template="default" />
            </div>
        </div>
    </div>
</div>