<?php

/*

type: layout

name: Simple Text

position: 6

*/
?>

<div class="page-section section pt-60 pb-80 edit safe-mode nodrop" field="layout-skin-6-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <div class="info-text center p-t-30 allow-drop">
                <h1 class="m-b-10">Page title</h1>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown
                    printer
                    took a
                    galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially
                    unchanged.
                </p>
            </div>
        </div>
    </div>
</div>
