<?php

/*

type: layout

name: Slider 2

position: 5

*/
?>

<div class="home-slider-section section edit safe-mode nodrop" field="layout-skin-5-<?php print $params['id'] ?>" rel="module">
    <module type="bxslider" template="background-overlay" controls="true" pager="false" />
</div>