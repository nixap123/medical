<?php

/*

type: layout

name: Login Block

position: 7

*/
?>

<div class="page-section section pt-60 pb-80 edit safe-mode nodrop" field="layout-skin-7-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
                <div class="login-reg-form">
                    <module type="users/login" template="skin-1"/>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>