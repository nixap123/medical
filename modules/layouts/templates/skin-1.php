<?php

/*

type: layout

name: Head Title

position: 1

*/
?>

<div class="edit safe-mode nodrop" field="layout-skin-1-<?php print $params['id'] ?>" rel="module">
    <div class="page-banner-section section mw-image-holder" style="background-image: url('<?php print template_url('img/'); ?>parallax-transp.jpg');">
        <img src="<?php print template_url('img/'); ?>parallax-transp.jpg" alt=""/>
        <span class="mw-image-holder-overlay"></span>
        <div class="mw-image-holder-content">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-banner-content-wrapper">
                            <div class="page-banner-content">
                                <?php if (category_title(CATEGORY_ID)) {
                                    $pageName = category_title(CATEGORY_ID);
                                } elseif (content_title(PAGE_ID)) {
                                    $pageName = content_title(PAGE_ID);
                                } else {
                                    $pageName = 'Title';
                                } ?>
                                <h1><?php print $pageName ?></h1>
                                <module type="breadcrumb" id="shop-breadcrumbs-<?php print PAGE_ID; ?>"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>