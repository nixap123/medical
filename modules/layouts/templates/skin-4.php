<?php

/*

type: layout

name: Posts

position: 4

*/
?>

<div class="blog-section section pb-80 edit safe-mode nodrop" field="layout-skin-4-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <module type="posts" template="skin-1" limit="2" />
        </div>
    </div>
</div>