<?php

/*

type: layout

name: Products

position: 3

*/
?>

<div class="product-section section pt-60 pb-120 edit safe-mode nodrop" field="layout-skin-3-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <module type="categories" content_id="<?php print PAGE_ID; ?>" template="horizontal-list-1"/>

            <module type="shop/products" limit="18" description-length="70"/>
        </div>
    </div>
</div>