<?php

/*

type: layout

name: Default

description: Default

*/
?>

<h5 class="widget-title">SIGN UP FOR OUR AWESOME NEWS</h5>

<form method="post" id="newsletters-form-<?php print $params['id'] ?>" class="sunscribe-form validate" novalidate>
    <?php print csrf_form(); ?>

    <div id="mc_embed_signup_scroll" class="hide-on-success">
        <label for="mce-EMAIL" class="hidden">Subscribe to our mailing list</label>
        <input type="email" value="" name="email" class="email" id="mce-EMAIL" placeholder="Email Address" required>
        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_6bbb9b6f5827bd842d9640c82_05d85f18ef" tabindex="-1" value=""></div>
        <div class="clear"><input type="submit" value="<?php _e('Subscribe'); ?>" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
        <br />
    </div>
</form>
