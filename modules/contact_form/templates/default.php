<?php

/*

type: layout

name: Default

description: Basic contact form

*/

?>


<form data-form-id="<?php print $form_id ?>" name="<?php print $form_id ?>" class="mw_form">
    <?php print csrf_form() ?>
    <input type="hidden" name="for" value="contact_form"/>
    <input type="hidden" name="for_id" value="<?php print $params['id']; ?>"/>

    <div class="row">
        <div class="col-sm-6 col-xs-12 mb-20"><input type="text" name="first_name" placeholder="Your name" class="form_input"></div>
        <div class="col-sm-6 col-xs-12 mb-20"><input type="text" name="last_name" placeholder="Last name" class="form_input"></div>
        <div class="col-sm-12 col-xs-12 mb-20"><input type="text" name="email" placeholder="Your email" class="form_input"></div>
        <div class="col-xs-12 mb-20"><textarea name="message" placeholder="Your message" class="form_textarea"></textarea></div>
        <?php if (get_option('disable_captcha', $params['id']) != 'y'): ?>
            <module type="captcha" />
        <?php endif; ?>
        <div class="col-xs-12"><input type="submit" value="<?php _e('Send'); ?>"></div>
    </div>
</form>

<div id="msg<?php print $form_id; ?>" style="display:none;">
    <div class="form-messege">
        <h2>Form has been sent</h2>
        <p>We will contact you soon with a responce.</p>
    </div>
</div>